package com.example.EurekaServer;

/**
 * https://spring.io/guides/gs/service-registration-and-discovery/
 * https://www.todaysoftmag.com/article/1429/micro-service-discovery-using-netflix-eureka
 * 
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class EurekaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurekaServerApplication.class, args);
	}
}
